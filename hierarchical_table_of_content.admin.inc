<?php
/**
 * @file
 * Administration pages for the 'hierarchical table of content' module.
 */

/**
 * Form builder; Settings page for the 'hierarchical table of content' module.
 *
 * @ingroup forms
 */
function hierarchical_toc_filter_admin_settings() {  
  $form['table_of_content_header_tags'] = array(
    '#type' => 'select',
    '#title' => t('Header tags'),
    '#description' => t('Select header tags you want to generate table of content for.'),
    '#options' => array(
      'h1' => 'h1',
      'h2' => 'h2',
      'h3' => 'h3',
      'h4' => 'h4',
      'h5' => 'h5',
      'h6' => 'h6',
    ),
    '#required' => 1,
    '#default_value' => variable_get('table_of_content_header_tags','h3'),
    '#multiple' => TRUE,
  );

  $form['table_of_content_ul_default_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Un-order list default title'),
    '#default_value' => variable_get('table_of_content_ul_default_title', '')
  );

  $form['table_of_content_ol_default_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Order list default title'),
    '#default_value' => variable_get('table_of_content_ol_default_title', '')
  );
   
  return system_settings_form($form);   
}
