/**
 * @file
 * Adds smooth scrolling to anchor links.
 * 
 */

(function ($) {

Drupal.hierarchical_tocScrollToOnClick = function() {
  // Make sure links still has hash.
  if (!this.hash || this.hash == '#') {
    return true;
  }

  // Make sure the href is pointing to an anchor link on this page.
  var href = this.href.replace(/#[^#]*$/, '');
  var url = window.location.toString();
  if (href && url.indexOf(href) === -1) {
    return true;
  }

  // Find hash target.
  var $a = $('a[name=' + this.hash.substring(1) + ']');

  // Make hash target is on the current page.
  if (!$a.length) {
    return true;
  }

  // Scroll to hash target
  var duration = Drupal.settings.hierarchical_toc_smooth_scroll_duration || 'medium';
  $('html, body').animate({scrollTop: $a.offset().top-50}, duration);
  return false;
}

Drupal.behaviors.hierarchical_tocSmoothScroll = {
  attach: function (context) {
    // Only map <a href="#..."> links
    $('a[href*="#"]', context).once('toc-filter').click(Drupal.hierarchical_tocScrollToOnClick);
  }
};

})(jQuery)
